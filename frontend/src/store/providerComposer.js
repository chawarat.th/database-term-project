import React, { cloneElement } from "react"
import { CounterProvider } from "./counterProvider"
import { ProductTypeProvider } from './productTypeStore'
import { ProductOrderProvider } from './productOrderProvider'

function ProviderComposer({ contexts, children }) {
    return contexts.reduce(
        (kids, parent) =>
            cloneElement(parent, {
                children: kids
            }),
        children
    )
}
export default function ContextProvider({ children }) {
    return (
        <ProviderComposer
            // add providers to array of contexts
            contexts={[
                <CounterProvider />,
                <ProductTypeProvider />,
                <ProductOrderProvider />
            ]}
        >
            {children}
        </ProviderComposer>
    )
}