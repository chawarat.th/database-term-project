/* eslint-disable default-case */
import React, { createContext, useReducer } from "react"

export const ProductTypeContext = createContext({})

const initialState = {
    product_type: {
        type_id: 0,
        type_name: ''

    },
}

const productTypeReducer = (state, action) => {
    console.log(action.payload)
    switch (action.type) {
        case "SELECT_TYPE":
            return {
                ...state, // copy state 
                product_type: action.payload
            }
        case "PRODUCT_TYPE_NAME":
            return {
                ...state, // copy state 
                product_type_name: action.payload
            }
    }
}

export const ProductTypeProvider = ({ children }) => {
    const [valueState, dispatch] = useReducer(
        productTypeReducer,
        initialState
    )

    const { productType } = valueState

    const storeProductType = payload =>
        dispatch({ type: "SELECT_TYPE", payload })

    return (
        <ProductTypeContext.Provider value={{ valueState, storeProductType }}>
            {children}
        </ProductTypeContext.Provider>
    )
}