/* eslint-disable array-callback-return */
/* eslint-disable default-case */
import React, { createContext, useReducer } from "react"

export const ProductOrderContext = createContext({})

const initialState = {
    products: []
}

const ProductOrderReducer = (state, action) => {
    // console.log(action.payload)
    switch (action.type) {
        case "SELECT_PRODUCT":
            let arr = state.products
            if (arr.length === 0) {
                arr.push({
                    product_id: action.payload.product_id,
                    quantity: action.couter,
                    product_description: action.payload.product_description,
                    product_image: action.payload.product_image,
                    product_name: action.payload.product_name,
                    product_price: action.payload.product_price
                })
            } else {
                let result = false
                let id = 0
                arr.map((item, index) => {
                    if (item.product_id === action.payload.product_id) {
                        result = true
                        id = index
                    }
                })
                if (result === true) {
                    arr[id].quantity = arr[id].quantity + action.couter
                } else if (result === false) {
                    arr.push({
                        product_id: action.payload.product_id,
                        quantity: action.couter,
                        product_description: action.payload.product_description,
                        product_image: action.payload.product_image,
                        product_name: action.payload.product_name,
                        product_price: action.payload.product_price
                    })
                }
            }
            return {
                // ...arr, // copy state 
                products: arr
            }

        case "CHANGE_QUANTITY":
            let arr2 = state.products
            arr2.map((item, index) => {
                if (item.product_id === action.payload.product_id) {
                    item.quantity = action.couter
                }
            })
            return {
                ...state, // copy state 
                product_type: arr2
            }

        case "REMOVE_ORDER":
            let orders = [...state.products]
            orders.splice(action.payload, 1)
            return {
                ...state, // copy state 
                products: orders
            }

    }
}

export const ProductOrderProvider = ({ children }) => {
    const [orderState, dispatch] = useReducer(
        ProductOrderReducer,
        initialState
    )

    // const { ProductOrder } = orderState

    const storeProductOrder = (payload, couter) =>
        dispatch({ type: "SELECT_PRODUCT", payload, couter })
    const changeQuantity = (payload, couter) =>
        dispatch({ type: "CHANGE_QUANTITY", payload, couter })
    const removeOrder = payload =>
        dispatch({ type: "REMOVE_ORDER", payload })

    return (
        <ProductOrderContext.Provider value={{ orderState, storeProductOrder, changeQuantity, removeOrder }}>
            {children}
        </ProductOrderContext.Provider>
    )
}