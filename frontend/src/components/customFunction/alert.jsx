import Swal from "sweetalert2";

export const alertSuccess = (text) => {
    Swal.fire({
        position: 'center',
        icon: 'success',
        type: 'success',
        title: 'สำเร็จ',
        text: text,
        showConfirmButton: false,
        timer: 2000
    });
}

export const alertError = () => {
    Swal.fire({
        position: 'center',
        icon: 'error',
        type: 'error',
        title: 'เกิดข้อผิดพลาด',
        text: 'กรุณาลองใหม่อีกครั้ง',
        showConfirmButton: false,
        timer: 2000
    });
}
export const alertErrorAdd = () => {
    Swal.fire({
        position: 'center',
        icon: 'error',
        type: 'error',
        title: 'บันทึกไม่สำเร็จ',
        text: 'กรุณาลองใหม่อีกครั้ง',
        showConfirmButton: false,
        timer: 2000
    });
}
export const alertConfirm = () => {
    return Swal.fire({
        position: 'center',
        icon: 'warning',
        type: 'warning',
        title: 'คุณแน่ใจว่าจะลบ ?',
        text: 'เมื่อคุณลบจะไม่สามารถเปลี่ยนแปลงได้อีก',
        showConfirmButton: true,
        cancelButtonText: 'ยกเลิก',
        confirmButtonText: 'ยืนยัน'
    });

}