import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';

const signOutFunction = {
    logout({ history }) {
        Swal.fire({
            title: 'ต้องการออกจากระบบ',
            text: 'กดยืนยันเพื่อลงชื่อออกจากระบบ',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#409d7e',
            cancelButtonColor: '#a4a7ae',
            cancelButtonText: 'ยกเลิก',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.isConfirmed) {
                const clearToken = new Promise((resolve, reject) => {
                    resolve(localStorage.clear());
                });
                const GotoPage = () => {
                    history.go('/login');
                };
                clearToken.then(GotoPage);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'ออกจากระบบสำเร็จ',
                    showConfirmButton: false,
                    timer: 2000
                })

            }
        }).catch(err => {
            Swal.fire({
                position: 'center',
                icon: 'error',
                type: 'error',
                title: 'เกิดข้อผิดพลาดระหว่างออกจากระบบ',
                text: 'ไม่สามารถออกจากระบบได้ กรุณาลองใหม่อีกครั้ง',
                showConfirmButton: false,
                timer: 2000
            });
        })
    },
    invalidToken() {
        Swal.fire({
            allowOutsideClick: false,
            title: 'หมดอายุการใช้งาน',
            text: 'กรุณาลงชื่อเข้าใช้ระบบใหม่ ก่อนการใช้งาน',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#409d7e',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.isConfirmed) {
                const clearToken = new Promise((resolve, reject) => {
                    resolve(localStorage.clear());
                });
                const GotoPage = () => {
                    window.location.assign('/login');
                };
                clearToken.then(GotoPage);
                // Swal.fire({
                //     position: 'center',
                //     icon: 'success',
                //     title: 'ออกจากระบบสำเร็จ',
                //     showConfirmButton: false,
                //     timer: 3000
                // });
            }
        });
    }
};

export default withRouter(signOutFunction);
