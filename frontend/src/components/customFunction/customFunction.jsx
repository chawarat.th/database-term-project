import moment from "moment"
import NumberFormat from "react-number-format"
import { Button, Icon } from "semantic-ui-react"

export const dateFormat = (date) => {
    return moment(date).format('L')
}

export const numberFormat = (num) => {
    return <NumberFormat value={num} displayType={'text'} thousandSeparator={true} />
}

export const ButtonUploadImage = ({ onClick, name, value }) => {
    return (
        <div>
            <span>
                <Button basic color='teal' icon labelPosition='left' className='label-btn-news'
                    onClick={() => handleClick(onClick, name)} style={{ marginBottom: '14px' }}>
                    <Icon name='upload' className='green-pri' />
                    เลือกภาพ
                </Button></span>
            {/* <Input id={name} name={name} input onChange={(e) => onClick(name, e.target.files[0])} > */}
            <input type='file' hidden id={name} name={name} onChange={(e) => onClick(name, e.target.files[0])}
                accept='image/png,image/jpeg'>
            </input>
            {/* </Input> */}
        </div>
    )
}

const handleClick = (onClick, name) => {
    document.getElementById(name).click()
}