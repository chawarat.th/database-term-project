import React, { useState } from 'react'
import { Button, Form, Grid, Header, Image, Segment } from 'semantic-ui-react'
import { HOST } from '../configs/host'
import Axios from 'axios'
import Swal from 'sweetalert2'
import LoginImg from '../images/login.svg'

function Login(props) {
    const [values, setValues] = useState({
        username: '',
        password: ''
    })

    const onSubmit = () => {
        Axios.post(HOST + 'login', values)
            .then(res => {
                let response = res.data.result
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    type: 'success',
                    title: 'เข้าสู่ระบบสำเร็จ',
                    // text: 'เข้าสู่ระบบสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    localStorage.setItem("accessToken", response.access_token);
                    localStorage.setItem("fullName", response.full_name);
                    localStorage.setItem("permission", response.permission);
                    if (response.permission === 'admin') {
                        props.history.push('/admin');
                    } else {
                        props.history.push('/');
                    }

                });
            })
            .catch(err => {
                if (err.response.data.message === "user-not-found") {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        type: 'error',
                        title: 'ไม่สามารถเข้าสู่ระบบได้',
                        text: 'ไม่มีผู้ใช้นี้ในระบบ',
                        showConfirmButton: false,
                        timer: 2000
                    });
                } else if (err.response.data.message === 'invalid-password') {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        type: 'warning',
                        title: 'รหัสผ่านไม่ถูกต้อง',
                        text: 'กรุณาลองใหม่อีกครั้ง',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            })
    }

    return (
        <>
            <Grid verticalAlign='middle' textAlign='center' style={{ height: '100vh' }}>
                <Grid.Column width='8'>
                    <Image src={LoginImg} />

                </Grid.Column>
                <Grid.Column width='5' verticalAlign='middle' textAlign='center'>
                    <Segment raised>
                        <Header as='h2'>Login</Header>
                        <Form className='text-left'>
                            <Form.Input
                                label='Username'
                                name='username'
                                onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                            />
                            <Form.Input
                                label='Password'
                                type='password'
                                name='password'
                                onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                            />
                        </Form>
                        <Grid columns='2'>
                            <Grid.Column style={{ padding: '20px 7px 0 0' }}>
                                <Button fluid color='teal' basic onClick={() => props.history.push('/sign-up')}>สมัครสมาชิก</Button>

                            </Grid.Column>
                            <Grid.Column style={{ padding: '20px 0 0 7px' }}>
                                <Button fluid color='teal' onClick={onSubmit}>Login</Button>

                            </Grid.Column>
                        </Grid>




                    </Segment>
                </Grid.Column>
            </Grid>
        </>
    )
}

export default Login
