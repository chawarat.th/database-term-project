import React, { useState } from 'react'
import { Button, Form, Grid, Header, Image, Segment } from 'semantic-ui-react'
import Swal from 'sweetalert2'
import { axios, REGISTER } from '../configs/userAPI'
import SignUpImg from '../images/signup.svg'
import { alertError } from './customFunction/alert'

function SignUp(props) {
    const [values, setValues] = useState({
        first_name: '',
        last_name: '',
        username: '',
        password: '',
        confirm_password: ''
    })
    const onSubmit = () => {
        axios.post(REGISTER, values)
            .then(res => {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    type: 'success',
                    title: 'สมัครสมาชิกสำเร็จ',
                    // text: 'เข้าสู่ระบบสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    props.history.push('/login');

                });
            })
            .catch(err => {
                if (err.response.data.message === "duplicate username") {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        type: 'error',
                        title: 'ไม่สามารถสมัครสมาชิกได้',
                        text: 'ชื่อผู้ใช้นี้มีในระบบแล้ว',
                        showConfirmButton: false,
                        timer: 2000
                    });
                } else if (err.response.data.message === 'password-not-match') {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        type: 'warning',
                        title: 'รหัสผ่านตรงกัน',
                        text: 'กรุณายืนยันรหัสผ่านให้ถูกต้อง',
                        showConfirmButton: false,
                        timer: 2000
                    });
                } else {
                    alertError()
                }
            })
    }
    return (
        <>
            <Grid verticalAlign='middle' textAlign='center' style={{ height: '100vh' }}>
                <Grid.Column width='8'>
                    <Image src={SignUpImg} />

                </Grid.Column>
                <Grid.Column width='5' verticalAlign='middle' textAlign='center'>
                    <Segment raised>
                        <Header as='h2'>Sign up</Header>
                        <Form className='text-left'>
                            <Form.Group widths='equal'>
                                <Form.Input
                                    fluid
                                    label='Firstname'
                                    name='first_name'
                                    onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                                />
                                <Form.Input
                                    fluid
                                    label='Lastname'
                                    name='last_name'
                                    onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                                />
                            </Form.Group>
                            <Form.Input
                                label='Username'
                                name='username'
                                onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                            />
                            <Form.Group widths='equal'>
                                <Form.Input
                                    fluid
                                    label='Password'
                                    type='password'
                                    name='password'
                                    onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                                />
                                <Form.Input
                                    fluid
                                    label='Confirm password'
                                    type='password'
                                    name='confirm_password'
                                    onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                                />
                            </Form.Group>




                            <Button fluid color='teal' onClick={onSubmit}>สมัครสมาชิก</Button>

                        </Form>

                    </Segment>
                </Grid.Column>
            </Grid>
        </>
    )
}

export default SignUp
