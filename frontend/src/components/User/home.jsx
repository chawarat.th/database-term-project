import React from 'react'
import { Grid } from 'semantic-ui-react'
import SideMenu from './sideMenu'
import Products from './products'

function Home() {
    return (
        <>

            <Grid textAlign='left'>
                <Grid.Column width='3'>
                    <SideMenu />
                </Grid.Column>
                <Grid.Column width='13'>

                    <Products />

                </Grid.Column>
            </Grid>
        </>
    )
}

export default Home
