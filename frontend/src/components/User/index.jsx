import React from 'react'
import { BrowserRouter as Router, Route, Switch, withRouter } from 'react-router-dom'
import Home from './home'
import Navbar from './navbar'
import ProductDetail from './productDetail'
import OrderList from './orderList'
import OrderHistory from './orderHistory'
import OrderDetail from './orderDetail'

function Index(props) {
    const path = props.match.url
    return (
        <div>
            <Router>
                <Navbar />

                <Switch>
                    <Route exact path={'/'} component={Home} />
                    <Route exact path={`${path}product-detail/:id`} component={ProductDetail} />
                    <Route exact path={`${path}order-list`} component={OrderList} />
                    <Route exact path={`${path}order-history`} component={OrderHistory} />
                    <Route exact path={`${path}order-detail/:id`} component={OrderDetail} />

                </Switch>
            </Router>

        </div>
    )
}

export default withRouter(Index)
