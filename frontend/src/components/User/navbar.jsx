/* eslint-disable array-callback-return */
import React, { useContext, useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import { Icon, Label, Menu } from 'semantic-ui-react'
import { ProductOrderContext } from '../../store/productOrderProvider'
import signOutFunction from '../customFunction/signOutFunction'

function Navbar(props) {
    const [activeItem] = useState('')
    const { orderState } = useContext(ProductOrderContext)
    const [count, setCount] = useState(0)
    const pathName = props.location.pathname
    const permission = localStorage.getItem('permission')


    useEffect(() => {
        let counter = 0
        orderState.products.map(item => {

            counter = counter + item.quantity
        })
        setCount(counter)

    }, [orderState])

    return (
        <>
            {pathName === '/login' || pathName === '/sign-up' ?
                null :
                permission === null || permission === 'user' ?
                    <Menu size='massive' secondary inverted fluid color='teal' style={{ margin: 0 }}>

                        <Menu.Item>
                            <img src='/images/wireframe/image.png' size='small' alt='' />
                        </Menu.Item>
                        <Menu.Item
                            as={Link}
                            position='right'
                            name='1'
                            active={pathName === '/'}
                            to={'/'}
                        >
                            <Icon name='home' /> หน้าแรก
                </Menu.Item>

                        <Menu.Item
                            as={Link}
                            name='2'
                            active={pathName === '/order-list'}
                            to={'/order-list'}
                        >
                            <Icon.Group>
                                <Icon name='shopping cart' />
                                <Icon corner='top right' size='tiny' >
                                    <Label circular className='label-basket'>
                                        {count}
                                    </Label>
                                </Icon>
                            </Icon.Group>


                        </Menu.Item>
                        {permission !== null &&
                            <Menu.Item
                                as={Link}
                                name='3'
                                active={pathName === '/order-history'}
                                to={'/order-history'}
                            >
                                <Icon name='list ul' /> ประวัติการซื้อ
                </Menu.Item>
                        }

                        {permission === null ?
                            <Menu.Item
                                name='4'
                                active={activeItem === '4'}
                                // onClick={(e, { name }) => setActiveItem(name)}
                                onClick={() => window.location.assign('/login')}
                            >
                                <Icon name='sign-in' /> เข้าสู่ระบบ
            </Menu.Item> :

                            <Menu.Item
                                name='4'
                                active={activeItem === '4'}
                                // onClick={(e, { name }) => setActiveItem(name)}
                                onClick={() => signOutFunction.logout(props)}
                            >
                                <Icon name='log out' /> ออกจากระบบ
                </Menu.Item>
                        }

                    </Menu>
                    :
                    <Menu size='massive' secondary inverted fluid color='teal' style={{ margin: 0 }}>

                        <Menu.Item
                            position='right'
                            name='4'
                            active={activeItem === '4'}
                            // onClick={(e, { name }) => setActiveItem(name)}
                            onClick={() => signOutFunction.logout(props)}
                        >
                            <Icon name='log out' /> ออกจากระบบ
                </Menu.Item>
                    </Menu>
            }
        </>
    )
}

export default withRouter(Navbar)
