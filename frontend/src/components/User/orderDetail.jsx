/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { Divider, Grid, Header, Item } from 'semantic-ui-react'
import { axios, ORDER_DETAIL } from '../../configs/userAPI';
import { dateFormat, numberFormat } from '../customFunction/customFunction';

function OrderDetail(props) {
    const orderId = props.match.params.id
    const [detail, setDetail] = useState({})
    const [orders, setOrders] = useState([])

    useEffect(() => {
        getOrderDetial()
    }, [])

    const getOrderDetial = () => {
        axios.get(ORDER_DETAIL + `?order_id=${orderId}`)
            .then(({ data }) => {
                setDetail(data.result)
                setOrders(data.result.order_details)
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    return (
        <>
            <Grid centered style={{ paddingTop: '20px' }}>
                <Grid.Column width='10'>
                    <Header as='h2'>รายละเอียดการซื้อ</Header>

                    <Grid >
                        <Grid.Row columns='3'>
                            <Grid.Column>
                                <Header sub>รหัสคำสั่งซื้อ</Header>
                                <span className='span-header'>{detail.ref_code}</span>
                            </Grid.Column>
                            <Grid.Column>
                                <Header sub>วันที่สั่งซื้อ</Header>
                                <span className='span-header'>{dateFormat(detail.order_date)}</span>
                            </Grid.Column>
                            <Grid.Column>
                                <Header sub>สถานะคำสั่งซื้อ</Header>
                                <span className='span-header'>{detail.status_name}</span>
                            </Grid.Column>
                        </Grid.Row>

                        <Grid.Row columns='3'>
                            <Grid.Column>
                                <Header sub>ผู้ส่ง</Header>
                                <span className='span-header'>{detail.full_name}</span>
                            </Grid.Column>
                            <Grid.Column className='custom-grid'>
                                <Header sub>ที่อยู่จัดส่ง</Header>
                                <span className='span-header'>{detail.order_address}</span>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Divider />

                    <Header as='h3' style={{ fontWeight: '500' }}>รายการสินค้า</Header>

                    <Item.Group divided>
                        {orders?.map(item => {
                            return <Item>
                                <Item.Image src={item.product_image} />

                                <Item.Content>

                                    <Grid style={{ height: '100%' }}>
                                        <Grid.Column width='13' style={{ paddingTop: 0 }}>
                                            <Item.Header style={{ fontSize: '18px' }}>{item.product_name}</Item.Header>
                                            <Item.Description className='descrip-detail'>{item.product_description}</Item.Description>
                                            <div>x {item.quantity}</div>
                                        </Grid.Column>
                                        <Grid.Column width='3' textAlign='right' verticalAlign='middle'>
                                            <div className='custom-descrip'>฿ {numberFormat(item.product_price)}</div>
                                        </Grid.Column>

                                        <Grid.Column width='16' textAlign='right' className='pd-tb-0'>
                                            <div className='custom-descrip'>รวม <span style={{ margin: ' 0 14px' }}>{numberFormat(item.product_price * item.quantity)} </span> บาท</div>
                                        </Grid.Column>
                                    </Grid>

                                </Item.Content>
                            </Item>
                        })}
                    </Item.Group>

                    <Divider />

                </Grid.Column>
                <Grid.Column width='10' textAlign='right' style={{ paddingTop: 0 }}>
                    <div className='price-detail'>ยอดคำสั่งซื้อทั้งหมด <span style={{ margin: ' 0 14px' }}>{numberFormat(detail.total_price)}</span> บาท</div>
                </Grid.Column>
            </Grid>
        </>
    )
}

export default OrderDetail
