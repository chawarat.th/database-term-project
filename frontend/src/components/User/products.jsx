/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useContext, useEffect } from 'react'
import { withRouter } from 'react-router'
import { Card, Header, Image, Grid, Pagination } from 'semantic-ui-react'
import { ALL_PRODUCT, axios } from '../../configs/userAPI'
import { ProductTypeContext } from '../../store/productTypeStore'

function Product(props) {
    const { valueState } = useContext(ProductTypeContext)
    const limit = 10
    const [page, setPage] = useState(1)
    const [totalPage, setTotalPage] = useState(1)
    const [products, setProducts] = useState([])
    const id = valueState.product_type.type_id
    const typeName = valueState.product_type.type_name
    const permission = localStorage.getItem('permission')


    useEffect(() => {
        if (permission === 'admin') {
            const clearToken = new Promise((resolve, reject) => {
                resolve(localStorage.clear());
            });
            const GotoPage = () => {
                window.location.assign('/');
            };
            clearToken.then(GotoPage);
        } else {
            getProducts()
        }

    }, [valueState.product_type, page])

    const getProducts = () => {
        axios({
            method: 'get',
            url: ALL_PRODUCT,
            params: {
                type_id: id,
                limit: limit,
                page: page
            }
        })
            .then(res => {
                const response = res.data.result
                let total = Math.ceil(response.total_rows / limit)
                setProducts(response.rows)
                setPage(response.page)
                setTotalPage(total)
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    return (
        <div>
            <Header as='h2' textAlign='center'>{typeName}</Header>
            <Grid stackable>
                {products?.map((item, index) => {
                    return <Grid.Column largeScreen='4' computer='4' tablet='8' key={index}>
                        <Card stackable fluid link
                            onClick={() => props.history.push({
                                pathname: '/product-detail/' + item.product_id
                            })}
                        >
                            <Image
                                src={item.product_image}
                                wrapped ui={false} />
                            <Card.Content>
                                <Card.Header>{item.product_name}</Card.Header>

                                <Card.Description className='price-detail'>
                                    ฿ {item.product_price}
                                </Card.Description>
                            </Card.Content>
                            <Card.Content extra>
                                คลัง: {item.product_stock}
                            </Card.Content>
                        </Card>
                    </Grid.Column>
                })}

                <Grid.Column width='16' textAlign='center'>
                    <Pagination
                        activePage={page}
                        firstItem={null}
                        lastItem={null}
                        siblingRange={1}
                        totalPages={totalPage}
                        onPageChange={(e, data) => setPage(data.activePage)}
                        secondary
                        ellipsisItem={undefined}
                    />
                </Grid.Column>
            </Grid>
        </div>
    )
}

export default withRouter(Product)
