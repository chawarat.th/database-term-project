import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Grid, Header, Label, Table } from 'semantic-ui-react'
import { axios, ORDER_LIST } from '../../configs/userAPI'
import { dateFormat, numberFormat } from '../customFunction/customFunction'

function OrderHistory(props) {
    const [orders, setOrders] = useState([])
    useEffect(() => {
        getOrderList()
    }, [])

    const getOrderList = () => {
        axios.get(ORDER_LIST)
            .then(({ data }) => {
                setOrders(data.result)
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    return (
        <>
            <Grid stackable centered style={{ paddingTop: '20px' }}>
                <Grid.Column width='12' textAlign='left'>
                    <Header as='h2'>ประวัติการซื้อของฉัน</Header>

                    <Table celled singleLine textAlign='center' selectable color='teal'>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell width='3'>รหัสคำสั่งซื้อ</Table.HeaderCell>
                                <Table.HeaderCell>วันที่สั่งซื้อ</Table.HeaderCell>
                                <Table.HeaderCell>ราคารวม (บาท)</Table.HeaderCell>
                                <Table.HeaderCell width='3'>สถานะคำสั่งซื้อ</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {orders?.map(item => {
                                return <Table.Row style={{ cursor: 'pointer' }}
                                    onClick={() => props.history.push({
                                        pathname: '/order-detail/' + item.order_id
                                    })}>
                                    <Table.Cell>{item.ref_code}</Table.Cell>
                                    <Table.Cell>{dateFormat(item.order_date)}</Table.Cell>
                                    <Table.Cell>{numberFormat(item.total_price)}</Table.Cell>
                                    <Table.Cell>
                                        <Label color='yellow' >{item.status_name}</Label>
                                    </Table.Cell>
                                </Table.Row>
                            })}
                        </Table.Body>
                    </Table>
                </Grid.Column>
            </Grid>
        </>
    )
}

export default withRouter(OrderHistory)
