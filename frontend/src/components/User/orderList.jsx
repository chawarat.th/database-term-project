/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import React, { useContext, useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Button, Form, Grid, Header, Icon, Image, Item, Segment } from 'semantic-ui-react'
import { axios, ORDER } from '../../configs/userAPI'
import { ProductOrderContext } from '../../store/productOrderProvider'
import { alertError, alertSuccess } from '../customFunction/alert'
import { numberFormat } from '../customFunction/customFunction'
import EmptyImg from '../../images/empty_order.svg'

function OrderList(props) {
    const { orderState, changeQuantity, removeOrder } = useContext(ProductOrderContext)
    const [totalPrice, setTotalPrice] = useState(0)
    const [values, setValues] = useState({
        status_id: 1,
        order_address_no: '',
        order_address_moo: '',
        order_address_street: '',
        order_address_sub_district: '',
        order_address_district: '',
        order_address_province: '',
        order_address_zipcode: '',
        total_price: 0,
        order_details: []
    })

    useEffect(() => {
        calTotalPrice()
    }, [orderState])


    const calTotalPrice = () => {
        let products = [...orderState.products]
        let total = 0
        let arr_order = []
        products.map(item => {
            total = total + (item.quantity * item.product_price)

            arr_order.push({
                product_id: item.product_id,
                quantity: item.quantity
            })
        })

        setValues({ ...values, total_price: total, order_details: arr_order })
    }
    const handleChange = (e, { name, value }) => {
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = () => {

        axios.post(ORDER, values)
            .then(res => {
                alertSuccess('ส่งคำสั่งซื้อเรียบร้อยแล้ว')
                window.location.assign('/')

            })
            .catch(err => {
                alertError()
            })

    }

    return (
        <>
            <Grid stackable centered style={{ paddingTop: '20px' }}>
                <Grid.Column width='12' textAlign='left'>
                    <Header as='h2'>สรุปคำสั่งซื้อ</Header>


                    <Grid>
                        <Grid.Column width='11'>
                            <Item.Group divided>
                                {orderState.products.length === 0 ?
                                    <Image src={EmptyImg} /> :
                                    orderState?.products.map((item, index) => {
                                        return <Item>
                                            <Item.Image src={item.product_image} />

                                            <Item.Content>
                                                <Item.Header>{item.product_name}</Item.Header>
                                                <Item.Meta>
                                                    <span className='cinema'>{item.product_description}</span>
                                                </Item.Meta>
                                                <Item.Description >
                                                    <div className='custom-descrip'>ราคาต่อชิ้น : {item.product_price} บาท</div>
                                                    <div className='custom-descrip'>จำนวน :
                                                    <Icon
                                                            className='i-default'
                                                            name='minus circle'
                                                            style={{ marginRight: '5px' }} link
                                                            onClick={() => changeQuantity(item, item.quantity > 1 ? item.quantity - 1 : 1)}
                                                        />
                                                        {item.quantity}
                                                        <Icon
                                                            className='i-default'
                                                            name='plus circle'
                                                            style={{ marginLeft: '5px' }} link
                                                            // onClick={() => setQuatity(quatity + 1)}
                                                            onClick={() => changeQuantity(item, item.quantity + 1)}

                                                        />

                                                    </div>
                                                    <div className='custom-descrip'>ราคารวม : {item.quantity * item.product_price} บาท</div>
                                                </Item.Description>
                                                <Item.Extra>
                                                    <Button floated='right' color='red' basic onClick={() => removeOrder(index)}>ลบ</Button>
                                                </Item.Extra>
                                            </Item.Content>
                                        </Item>
                                    })}

                            </Item.Group>
                        </Grid.Column>
                        <Grid.Column width='5'>
                            <Segment color='teal'>
                                <Header>ข้อมูลคำสั่งซื้อ</Header>
                                <Form style={{ paddingTop: '20px' }}>
                                    <Form.Group widths='equal'>
                                        <Form.Input
                                            label='บ้านเลขที่'
                                            fluid
                                            name='order_address_no'
                                            onChange={handleChange}
                                        />
                                        <Form.Input
                                            label='หมู่'
                                            fluid
                                            name='order_address_moo'
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                    <Form.Group widths='equal'>
                                        <Form.Input
                                            label='ถนน'
                                            fluid
                                            name='order_address_street'
                                            onChange={handleChange}
                                        />
                                        <Form.Input
                                            label='ตำบล'
                                            fluid
                                            name='order_address_sub_district'
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                    <Form.Group widths='equal'>
                                        <Form.Input
                                            label='อำเภอ'
                                            fluid
                                            name='order_address_district'
                                            onChange={handleChange}
                                        />
                                        <Form.Input
                                            label='จังหวัด'
                                            fluid
                                            name='order_address_province'
                                            onChange={handleChange}
                                        />
                                    </Form.Group>
                                    <Form.Input
                                        width='8'
                                        label='รหัสไปรษณีย์'
                                        fluid
                                        name='order_address_zipcode'
                                        onChange={handleChange}
                                    />

                                </Form>

                                <Header textAlign='right'>
                                    ราคารวมทั้งหมด
                                    <span style={{ margin: ' 0 14px' }}>{numberFormat(values.total_price)}</span>
                                     บาท
                                </Header>

                                <Button fluid size='large' color='teal' onClick={handleSubmit}>ยืนยันคำสั่งซื้อ</Button>
                            </Segment>
                        </Grid.Column>
                    </Grid>


                </Grid.Column>
            </Grid>
        </>
    )
}

export default withRouter(OrderList)
