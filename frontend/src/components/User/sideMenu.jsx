/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import React, { useContext, useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Menu } from 'semantic-ui-react'
import { axios, PRODUCT_TYPE } from '../../configs/userAPI'
import { ProductTypeContext } from '../../store/productTypeStore'

function SideMenu() {
    const [activeItem, setActiveItem] = useState(0)
    const [types, setTypes] = useState([])
    const { storeProductType } = useContext(ProductTypeContext)

    useEffect(() => {
        getProductTypes()
    }, [])

    const getProductTypes = () => {
        axios.get(PRODUCT_TYPE)
            .then(res => {
                setTypes(res.data.result)
                storeProductType({
                    type_id: res.data.result[0].type_id,
                    type_name: res.data.result[0].type_name
                })
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    return (
        <div>
            <Menu vertical size='huge' fluid className='text-left'>
                <Menu.Item>
                    <Menu.Header className='header-category'>ประเภทสินค้า</Menu.Header>

                    <Menu.Menu>
                        {types?.map((item, index) => {
                            return <Menu.Item
                                key={index}
                                name={item.type_name}
                                active={activeItem === index}
                                onClick={() => {
                                    setActiveItem(index)
                                    storeProductType({
                                        type_id: item.type_id,
                                        type_name: item.type_name
                                    })
                                }}
                            />
                        })}
                    </Menu.Menu>
                </Menu.Item>
            </Menu>
        </div>
    )
}

export default withRouter(SideMenu)
