/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react'
import { withRouter } from 'react-router'
import { Button, Grid, Header, Icon, Image, Input, Segment } from 'semantic-ui-react'
import { axios, PRODUCT } from '../../configs/userAPI'
import { CounterContext } from '../../store/counterProvider'
import { ProductOrderContext } from '../../store/productOrderProvider'

function ProductDetail(props) {
    const productId = props.match.params.id
    const [product, setProduct] = useState({})
    const [quatity, setQuatity] = useState(1)
    const { addCounter } = useContext(CounterContext)
    const { storeProductOrder } = useContext(ProductOrderContext)


    useEffect(() => {
        getProduct()
    }, [])

    const getProduct = () => {
        axios.get(PRODUCT + '?product_id=' + productId)
            .then(({ data }) => {
                setProduct(data.result)
            })
            .catch(err => {
                console.log(err);
            })
    }
    return (
        <div >
            <Grid centered>
                <Grid.Column width='14' >
                    <Segment color='teal'>
                        <Grid stretched>
                            <Grid.Column width='6'>
                                <Image src={product.product_image} />
                            </Grid.Column>
                            <Grid.Column width='10'>


                                <Grid verticalAlign='middle' >
                                    <Grid.Row>
                                        <Header as='h1' style={{ width: '100%' }}>{product.product_name}</Header>
                                        <div>{product.product_description}</div>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <div className='price'>฿ {product.product_price}</div>

                                    </Grid.Row>
                                    <Grid.Row style={{ paddingTop: '20px' }}>
                                        <Grid.Column width='3' className='pd-lr-0'>
                                            จำนวน
                                    </Grid.Column>
                                        <Grid.Column width='3' className='pd-lr-0'>
                                            <Input fluid style={{ alignItems: 'center' }} >
                                                <Icon
                                                    className='i-default'
                                                    size='large' name='minus circle'
                                                    style={{ marginRight: '3px' }} link
                                                    onClick={() => setQuatity(quatity > 1 ? quatity - 1 : 1)}
                                                />
                                                <input value={quatity} onChange={(e) => setQuatity(Number(e.target.value))} />
                                                <Icon
                                                    className='i-default'
                                                    size='large' name='plus circle'
                                                    style={{ marginLeft: '3px' }} link
                                                    onClick={() => setQuatity(quatity + 1)}
                                                />
                                            </Input>
                                        </Grid.Column>
                                        <Grid.Column width='10'>
                                            มีสินค้าทั้งหมด {product.product_stock} ชิ้น
                                    </Grid.Column>

                                        <Grid.Column width='5' className='pd-bt-group'>
                                            <Button
                                                fluid size='large'
                                                basic color='teal'
                                                onClick={() => {
                                                    addCounter(quatity)
                                                    storeProductOrder(product, quatity)
                                                }}
                                            >เพิ่มไปยังรถเข็น</Button>
                                        </Grid.Column>
                                        <Grid.Column width='5' className='pd-bt-group'>
                                            <Button fluid size='large' color='teal'
                                                onClick={() => {
                                                    props.history.push('/order-list')
                                                    addCounter(quatity)
                                                    storeProductOrder(product, quatity)
                                                }}>ซื้อสินค้า</Button>

                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>



                            </Grid.Column>
                        </Grid>
                    </Segment>

                </Grid.Column>
            </Grid>
        </div>
    )
}

export default withRouter(ProductDetail)
