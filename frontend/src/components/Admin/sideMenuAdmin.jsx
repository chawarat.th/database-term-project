import React from 'react'
import { withRouter } from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

function SideMenuAdmin(props) {
    const pathName = props.location.pathname
    return (
        <div>
            <Menu vertical size='huge' fluid className='text-left' color='teal'>
                <Menu.Item
                    name='จัดการสินค้า'
                    active={pathName === '/admin'}
                    onClick={() => {
                        props.history.push('/admin')
                    }}


                />
                <Menu.Item
                    name='จัดการคำสั่งซื้อ'
                    active={pathName === '/admin/orders'}
                    onClick={() => {
                        props.history.push('/admin/orders')
                    }}

                />
            </Menu>
        </div>
    )
}

export default withRouter(SideMenuAdmin)
