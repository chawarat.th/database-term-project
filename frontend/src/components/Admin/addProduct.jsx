/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Button, Form, Grid, Header, Icon, Image, Input, Segment } from 'semantic-ui-react'
import { ADMIN_POST_PRODUCT, axios } from '../../configs/adminApi'
import { PRODUCT_TYPE } from '../../configs/userAPI'
import { alertErrorAdd, alertSuccess } from '../customFunction/alert'
import { ButtonUploadImage } from '../customFunction/customFunction'

function AddProduct(props) {
    const [showImage, setShowImage] = useState([])
    const [quatity, setQuatity] = useState(1)
    const [values, setValues] = useState({})
    const [type, setType] = useState([])

    useEffect(() => {
        getProductType()
    }, [])

    const getProductType = () => {
        axios.get(PRODUCT_TYPE)
            .then(({ data }) => {
                let arr = data.result
                arr.shift()
                setType(arr)
            })
            .catch(err => {
                console.log(err.response)
            })
    }

    const handleUploadImage = (name, value) => {
        let arr_file = []
        arr_file.push(value)

        arr_file.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                setShowImage([reader.result])
            }
            reader.readAsDataURL(file);
        })
        setValues({ ...values, product_image: arr_file })

    }

    useEffect(() => {
        setValues({ ...values, product_stock: quatity })
    }, [quatity])

    const handleSubmit = () => {
        const formData = new FormData()
        formData.append('product_image', values.product_image[0])
        formData.append('product_stock', values.product_stock)
        formData.append('product_description', values.product_description)
        formData.append('product_name', values.product_name)
        formData.append('product_price', values.product_price)
        formData.append('type_id', values.type_id)

        axios.post(ADMIN_POST_PRODUCT, formData)
            .then(res => {
                alertSuccess('ข้อมูลได้ถูกบันทึกแล้ว')
                props.history.goBack()
            })
            .catch(err => {
                alertErrorAdd()
            })
    }


    return (
        <>
            <Header as='h2'>เพิ่มสินค้า</Header>
            <Segment color='teal' fluid>

                <Grid >
                    <Grid.Column width='4'>
                        {showImage?.map((item, index) => {
                            return <Image src={item} size='medium' className='set-h-image-cover-news' />
                        })}
                        <div style={{ paddingTop: '14px' }}>
                            <ButtonUploadImage
                                name="image"
                                onClick={handleUploadImage}
                            // value={''}
                            />
                        </div>

                    </Grid.Column>
                    <Grid.Column width='12'>
                        <Form>
                            <Form.Input
                                label='ชื่อสินค้า'
                                name='product_name'
                                onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                            />
                            <Form.Group>
                                <Form.Input
                                    width='4'
                                    label='ราคา'
                                    type='number'
                                    name='product_price'
                                    onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                                />
                                <Form.Select
                                    width='12'
                                    label='ประเภทสินค้า'
                                    options={type?.map(item => ({
                                        text: item.type_name,
                                        value: item.type_id,
                                        key: item.type_id
                                    }))}
                                    placeholder='กรุณาเลือก'
                                    name='type_id'
                                    onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                                />
                            </Form.Group>

                            <Form.TextArea
                                label='รายละเอียด'
                                name='product_description'
                                onChange={(e, { name, value }) => setValues({ ...values, [name]: value })}
                            />

                            <Form.Field width='4'>
                                <label>จำนวน</label>
                                <Input fluid style={{ alignItems: 'center' }} >
                                    <Icon
                                        className='i-default'
                                        size='large' name='minus circle'
                                        style={{ marginRight: '3px' }} link
                                        onClick={() => setQuatity(quatity > 1 ? quatity - 1 : 1)}
                                    />
                                    <input value={quatity} onChange={(e) => setQuatity(Number(e.target.value))} />
                                    <Icon
                                        className='i-default'
                                        size='large' name='plus circle'
                                        style={{ marginLeft: '3px' }} link
                                        onClick={() => setQuatity(quatity + 1)}
                                    />
                                </Input>
                            </Form.Field>


                        </Form>
                    </Grid.Column>
                    <Grid.Column width='16' textAlign='right'>
                        <Button color='teal' onClick={handleSubmit}>บันทึก</Button>
                    </Grid.Column>
                </Grid>
            </Segment>
        </>
    )
}

export default withRouter(AddProduct)
