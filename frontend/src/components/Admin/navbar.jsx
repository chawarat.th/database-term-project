import React from 'react'
import { withRouter } from 'react-router-dom'
import { Icon, Menu } from 'semantic-ui-react'
import signOutFunction from '../customFunction/signOutFunction'

function Navbar(props) {
    return (
        <>
            <Menu size='massive' secondary inverted fluid color='teal' style={{ margin: 0 }}>
                <Menu.Item>
                    <img src='/images/wireframe/image.png' size='small' alt='' />
                </Menu.Item>
                <Menu.Item
                    // as={Link}
                    position='right'
                    name='1'
                    // active={pathName === '/'}
                    onClick={() => signOutFunction.logout(props)}
                >
                    <Icon name='home' /> หน้าแรก
                </Menu.Item>
            </Menu>
        </>
    )
}

export default withRouter(Navbar)
