import React from 'react'
import { BrowserRouter as Router, Route, Switch, withRouter } from 'react-router-dom'
import { Grid } from 'semantic-ui-react';
import Orders from './orderList'
import ProductList from './productList'
import SideMenu from './sideMenuAdmin'
import AddProduct from './addProduct'
import OrderDetail from './orderDetail'
import EditProduct from './editProduct'

function Index(props) {
    return (
        <div>

            <Router>
                {/* <Navbar /> */}
                <Grid centered textAlign='left'>
                    <Grid.Column width='3'>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column width='10' style={{ paddingTop: '20px' }}>

                        <Switch>
                            <Route exact path={'/admin'} component={ProductList} />
                            <Route exact path={'/admin/orders'} component={Orders} />
                            <Route exact path={'/admin/add-product'} component={AddProduct} />
                            <Route exact path={'/admin/order-detail/:id'} component={OrderDetail} />
                            <Route exact path={'/admin/edit-product/:id'} component={EditProduct} />
                        </Switch>

                    </Grid.Column>
                </Grid>


            </Router>
        </div>
    )
}

export default withRouter(Index)
