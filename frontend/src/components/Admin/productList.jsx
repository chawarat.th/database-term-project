/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Button, Grid, Header, Icon, Pagination, Table } from 'semantic-ui-react'
import { ADMIN_PRODUCTS, axios, ADMIN_DEL_PRODUCT } from '../../configs/adminApi'
import { alertConfirm, alertError, alertSuccess } from '../customFunction/alert'
import { numberFormat } from '../customFunction/customFunction'

function ProductList(props) {
    const [products, setProducts] = useState([])
    const limit = 10
    const [page, setPage] = useState(1)
    const [totalPage, setTotalPage] = useState(1)

    useEffect(() => {
        getProducts()
    }, [page])

    const getProducts = () => {
        axios({
            method: 'get',
            url: ADMIN_PRODUCTS,
            params: {
                limit: limit,
                page: page
            }
        })
            .then(({ data }) => {
                const response = data.result
                let total = Math.ceil(response.total_rows / limit)
                setProducts(response.rows)
                setPage(response.page)
                setTotalPage(total)
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    const handleDelete = (id) => {
        alertConfirm()
            .then(async (result) => {
                if (result.value) {
                    await axios.delete(ADMIN_DEL_PRODUCT + '?product_id=' + id)
                        .then(res => {
                            alertSuccess('ข้อมูลได้ถูกลบแล้ว')
                            getProducts()
                        })
                        .catch(err => {
                            alertError()
                        })
                }
            })
    }
    return (
        <>
            <Grid verticalAlign='middle'>
                <Grid.Column width='13' className='pd-lr-0'>
                    <Header as='h2'>ราการสินค้า</Header>
                </Grid.Column>
                <Grid.Column width='3' className='pd-lr-0'>
                    <Button fluid color='teal' onClick={() => props.history.push('/admin/add-product')}>
                        <Icon name='plus' />เพิ่มสินค้า
                        </Button>
                </Grid.Column>
            </Grid>

            <Table celled selectable textAlign='center'>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width='1'>ลำดับ</Table.HeaderCell>
                        <Table.HeaderCell textAlign='left'>ชื่อสินค้า</Table.HeaderCell>
                        <Table.HeaderCell collapsing>คลังสินค้า (ชิ้น)</Table.HeaderCell>
                        <Table.HeaderCell collapsing>ราคา (บาท)</Table.HeaderCell>
                        <Table.HeaderCell >จัดการ</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {products?.map((item, index) => {
                        return <Table.Row key={index} >
                            <Table.Cell>{(page - 1) * 10 + index + 1}</Table.Cell>
                            <Table.Cell textAlign='left'>{item.product_name}</Table.Cell>
                            <Table.Cell>{numberFormat(item.product_stock)}</Table.Cell>
                            <Table.Cell>{numberFormat(item.product_price)}</Table.Cell>
                            <Table.Cell collapsing>
                                <Button
                                    icon='edit'
                                    size='tiny'
                                    color='blue'
                                    basic
                                    onClick={() => props.history.push({
                                        pathname: '/admin/edit-product/' + item.product_id
                                    })}
                                />
                                <Button icon='trash' size='tiny' color='red' basic onClick={() => handleDelete(item.product_id)} />
                            </Table.Cell>
                        </Table.Row>
                    })}

                </Table.Body>
            </Table>
            <Grid>
                <Grid.Column width='16' textAlign='center'>
                    <Pagination
                        activePage={page}
                        firstItem={null}
                        lastItem={null}
                        siblingRange={1}
                        totalPages={totalPage}
                        onPageChange={(e, data) => setPage(data.activePage)}
                        secondary
                        ellipsisItem={undefined}
                    />
                </Grid.Column>
            </Grid>
        </>
    )
}

export default withRouter(ProductList)
