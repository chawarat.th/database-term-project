import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Button, Dropdown, Header, Icon, Table } from 'semantic-ui-react'
import { ADMIN_ORDERS, axios, ADMIN_ORDER_STATUS, ADMIN_PUT_ORDER_STATUS } from '../../configs/adminApi'
import { dateFormat } from '../customFunction/customFunction'

function Orders(props) {
    const [orders, setOrders] = useState([])
    const [orderStatus, setOrderStatus] = useState([])

    useEffect(() => {
        getOrders()
        getOrderStatus()
    }, [])

    const getOrders = () => {
        axios.get(ADMIN_ORDERS)
            .then(({ data }) => {
                setOrders(data.result)
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    const getOrderStatus = () => {
        axios.get(ADMIN_ORDER_STATUS)
            .then(({ data }) => {
                setOrderStatus(data.result)
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    const changeOrderStatus = (statusId, orderId) => {
        axios.put(ADMIN_PUT_ORDER_STATUS + `?status_id=${statusId}&order_id=${orderId}`)
            .then(({ data }) => {
                getOrders()
            })
            .catch(err => {
                console.log(err.response);
            })
    }
    return (
        <>
            <Header as='h2'>คำสั่งซื้อทั้งหมด</Header>

            <Table celled selectable textAlign='center'>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width='1'>ลำดับ</Table.HeaderCell>
                        <Table.HeaderCell width='3'>รหัสคำสั่งซื้อ</Table.HeaderCell>
                        <Table.HeaderCell>วันที่สั่งซื้อ</Table.HeaderCell>
                        <Table.HeaderCell>เปลี่ยนสถานะ</Table.HeaderCell>
                        <Table.HeaderCell >เพิ่มเติม</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {orders?.map((item, index) => {
                        return <Table.Row key={index} >
                            <Table.Cell>{index + 1}</Table.Cell>
                            <Table.Cell>{item.ref_code}</Table.Cell>
                            <Table.Cell>{dateFormat(item.order_date)}</Table.Cell>
                            <Table.Cell>
                                {/* {item.status_name} */}
                                <Dropdown
                                    inline
                                    value={item.status_id}
                                    options={orderStatus?.map(status => ({
                                        text: status.status_name,
                                        value: status.status_id,
                                        key: status.status_id
                                    }))}
                                    onChange={(e, { value }) => changeOrderStatus(value, item.order_id)}
                                />
                            </Table.Cell>
                            <Table.Cell collapsing>
                                <Button icon color='teal' basic
                                    onClick={() => props.history.push({
                                        pathname: '/admin/order-detail/' + item.order_id
                                    })}
                                >
                                    <Icon name='search' /> ดูรายละเอียด
                                </Button></Table.Cell>
                        </Table.Row>
                    })}

                </Table.Body>
            </Table>
        </>
    )
}

export default withRouter(Orders)
