import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Index from './components/User/home'
import Login from './components/login'
import ProductDetail from './components/User/productDetail'
import Navbar from './components/User/navbar';
import Admin from './components/Admin/index'
import OrderList from './components/User/orderList'
import OrderHistory from './components/User/orderHistory'
import OrderDetail from './components/User/orderDetail'
import SignUp from './components/signUp'

export default function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/login" exact component={Login} />
          <Route path="/sign-up" exact component={SignUp} />
          <Route path="/" exact component={Index} />
          <Route exact path={`/product-detail/:id`} component={ProductDetail} />
          <Route exact path={`/order-list`} component={OrderList} />
          <Route exact path={`/order-history`} component={OrderHistory} />
          <Route exact path={`/order-detail/:id`} component={OrderDetail} />
          <Route path="/admin" component={Admin} />

        </Switch>
      </Router>

    </div>
  );
}

const LoginRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        localStorage.getItem("accessToken") ? (
          <Redirect
            to='/login'
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

const ProtectRoute = ({ component: Component, ...rest }) => {
  return <Route {...rest} render={(props) => localStorage.getItem("accessToken")
    ? <Component {...props} />
    : <Redirect to='/login' />
  } />;
};

