import { HOST } from './host'
import axiosApi from 'axios'
import signOutFunction from '../components/customFunction/signOutFunction'

export const axios = axiosApi.create({
    baseURL: HOST
});
axios.interceptors.request.use((request) => {
    request.headers.Authorization = `Bearer ${localStorage.getItem('accessToken')}`;
    return request;
});
// function เช็ค 401 ออกจากระบบ
axios.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        if (error.response) {
            if (error.response.status === 401) {
                // signOutFunction.invalidToken()
                window.location.assign('/login');

            } else {
                return Promise.reject(error);
            }
        } else {
            //console.log('connect internet');
        }
    }
);


export const ADMIN_ORDERS = `admin_order/list`
export const ADMIN_ORDER_STATUS = `admin_order/dropdown/status`
export const ADMIN_PUT_ORDER_STATUS = `admin_order/status`
export const ADMIN_PRODUCTS = `admin_product/all`
export const ADMIN_PRODUCT_LIST = `admin_order/detail`
export const ADMIN_POST_PRODUCT = `admin_product/insert_product`
export const ADMIN_GET_PRODUCT = `product/product_by_id`
export const ADMIN_PUT_PRODUCT = `admin_product/update_product`
export const ADMIN_DEL_PRODUCT = `admin_product/delete_product`
