import { HOST } from './host'
import axiosApi from 'axios'
import signOutFunction from '../components/customFunction/signOutFunction'

export const axios = axiosApi.create({
    baseURL: HOST
});
axios.interceptors.request.use((request) => {
    request.headers.Authorization = `Bearer ${localStorage.getItem('accessToken')}`;
    return request;
});
// function เช็ค 401 ออกจากระบบ
axios.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        if (error.response) {
            if (error.response.status === 401) {
                // signOutFunction.invalidToken()
                window.location.assign('/login');

            } else {
                return Promise.reject(error);
            }
        } else {
            //console.log('connect internet');
        }
    }
);


export const PRODUCT_TYPE = `product/types`
export const ALL_PRODUCT = `product/all`
export const PRODUCT = `product/product_by_id`
export const ORDER = `order/insert_order`
export const ORDER_LIST = `order/list`
export const ORDER_DETAIL = `order/detail`
export const REGISTER = `register`