#!/usr/bin/env bash
openssl genrsa  -out private.pem 2048
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
openssl rsa -in private.pem -out private_unencrypted.pem -outform PEM
mv private.pem key.rsa && mv public.pem key.rsa.pub
echo "DONE.."