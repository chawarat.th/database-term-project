package main

import (
	"database-term-project/db"
	"database-term-project/routes"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"os"
)

func main() {
	// initial cofig
	db.Init("prod") //for dev

	// initial echo
	e := echo.New()
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))
	e.HideBanner = true
	// route of api
	routes.Route(e)
	// start api set port
	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
	return
}
