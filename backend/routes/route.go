package routes

import (
	"database-term-project/controllers/Authentication"
	"database-term-project/controllers/Member"
	"database-term-project/controllers/Order"
	"database-term-project/controllers/Order/Admin"
	"database-term-project/controllers/Product"
	"database-term-project/controllers/ProductType"
	"database-term-project/middlewares"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Route(e *echo.Echo) {
	//initial cors origin middleware
	login := middlewares.SetLogin()
	middlewares.SetCORS(e)
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "AccessTime => ${time_rfc3339_nano}\nHost => ${host}, RemoteIP => ${remote_ip},\nMethod => ${method},\nURI => ${uri}, Status => ${status},\nError => ${error},\nUserAgent => ${user_agent}\n--------------\n",
		Output: e.Logger.Output(),
	}))
	e.Static("/static", "static")
	route := e.Group("/api/v1")
	route.POST("/register", Member.Registration)
	route.POST("/login", Authentication.Login)

	userAdmin := route.Group("/admin_user", login,middlewares.Admin)
	userAdmin.GET("/all_user", Member.GetAllMember)
	userAdmin.POST("/update_user_status", Member.UpdateUserStatus)
	userAdmin.GET("/dropdown/status_user", Member.GetAllUserStatus)

	productAdminSystem := route.Group("/admin_product", login, middlewares.Admin)
	productAdminSystem.POST("/insert_product", Product.AddProduct)
	productAdminSystem.DELETE("/delete_product", Product.DeleteProduct)
	productAdminSystem.PUT("/update_product", Product.UpdateProduct)
	productAdminSystem.GET("/all", Product.GetAllProductAdmin)

	productSystem := route.Group("/product")
	productSystem.GET("/all", Product.GetAllProducts)
	productSystem.GET("/product_by_id", Product.GetProductById)

	productSystem.GET("/types", ProductType.GetAllProductType)
	////////////////////////////////////////////////
	orderAdminSystem := route.Group("/admin_order", login, middlewares.Admin)
	orderAdminSystem.GET("/list", Admin.GetOrderListAdmin)
	orderAdminSystem.GET("/detail", Order.GetOrderDetail)
	orderAdminSystem.DELETE("/cancel", Admin.CancelOrder)
	orderAdminSystem.PUT("/status", Admin.UpdateStatus)
	orderAdminSystem.GET("/dropdown/status", Admin.GetAllOrderStatus)

	orderSystem := route.Group("/order", login, middlewares.Member)
	orderSystem.POST("/insert_order", Order.CreateOrder)
	orderSystem.GET("/detail", Order.GetOrderDetail)
	orderSystem.GET("/list", Order.GetOrderListByUserId)


}
