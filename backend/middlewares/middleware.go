package middlewares

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"database-term-project/config"
	"database-term-project/models"
)

func SetCORS(e *echo.Echo) {
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))
}

//SetLogin ตัวกลางเช็ค Login
func SetLogin() echo.MiddlewareFunc {
	_, publicKey := config.GetKey()
	return middleware.JWTWithConfig(middleware.JWTConfig{
		TokenLookup:   "header:" + echo.HeaderAuthorization,
		SigningMethod: "RS256",
		AuthScheme:    "Bearer",
		Claims:        &models.TokenClaim{},
		SigningKey:    publicKey,
	})
}

func Admin(next echo.HandlerFunc) echo.HandlerFunc {
	//check permission form database
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(*models.TokenClaim)
		isMember := claims.Permission.User
		isAdmin := claims.Permission.Admin
		fmt.Println(isMember)
		fmt.Println(isAdmin)
		//
		if isAdmin == "active" && isMember == "active" {
			fmt.Println("Welcome Admin")
		} else {
			return echo.ErrUnauthorized
		}
		return next(c)
	}
}

func Member(next echo.HandlerFunc) echo.HandlerFunc {
	//check permission form database
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(*models.TokenClaim)
		isMember := claims.Permission.User
		//
		if isMember == "active" {
			fmt.Println("Welcome Member")
		} else {
			return echo.ErrUnauthorized
		}
		return next(c)
	}
}

