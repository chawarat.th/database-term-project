package models

import "time"

type Products struct {
	ProductId          int        `gorm:"primaryKey;" json:"product_id"`
	ProductName        string     `json:"product_name"`
	ProductPrice       int        `json:"product_price"`
	ProductDescription string     `json:"product_description"`
	ProductStock       int        `json:"product_stock"`
	ProductImage       string     `json:"product_image"`
	TypeId             int        `json:"type_id"`
	CreatedAt          time.Time  `json:"created_at"`
	UpdatedAt          time.Time  `json:"updated_at"`
	DeletedAt          *time.Time `json:"deleted_at"`
}

type ProductTypes struct {
	TypeId    int        `gorm:"primaryKey;" json:"type_id"`
	TypeName  string     `json:"type_name"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
}

type ProductRes struct {
	ProductId          int        `gorm:"primaryKey;" json:"product_id"`
	ProductName        string     `json:"product_name"`
	ProductPrice       int        `json:"product_price"`
	ProductStock       int        `json:"product_stock"`
}
