package models

import "time"

type FileResponse struct {
	ID        int        `gorm:"PRIMARY_KEY;autoIncrement:true" json:"id"`
	Name      string     `json:"name"`
	Path      string     `json:"path"`
	Type      string     `json:"type"`
	ResultId  int        `json:"result_id"`
	CreatedAt time.Time  `json:"created_at,omitempty"`
	UpdatedAt time.Time  `json:"updated_at,omitempty"`
	DeletedAt *time.Time `sql:"index"`
	CreatedBy string     `json:"created_by,omitempty"`
	UpdatedBy string     `json:"updated_by"`
	DeletedBy string     `json:"deleted_by"`
}
