package models

type BindRegister struct {
	FullName        string `json:"full_name,omitempty"`
	FirstName       string `json:"first_name,omitempty"`
	LastName        string `json:"last_name,omitempty"`
	Username        string `json:"username,omitempty"`
	Password        string `json:"password,omitempty"`
	Position        string `json:"position,omitempty"`
	ConfirmPassword string `json:"confirm_password,omitempty"`
}
