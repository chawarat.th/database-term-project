package models

type Pagination struct {
	TotalRows    int         `json:"total_rows,omitempty"`
	Limit        int         `json:"limit,omitempty"`
	Page         int         `json:"page,omitempty"`
	FirstPage    string      `json:"first_page,omitempty"`
	PreviousPage string      `json:"previous_page,omitempty"`
	NextPage     string      `json:"next_page,omitempty"`
	LastPage     string      `json:"last_page,omitempty"`
	Rows         interface{} `json:"rows,omitempty"`
}
