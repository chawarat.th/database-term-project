package models

import "time"

type Permission struct {
	PermissionId int    `gorm:"PRIMARY_KEY" json:"permission_id,omitempty"`
	User         string `gorm:"default:'active'" json:"user"`
	Admin        string `gorm:"default:'non-active'" json:"admin,omitempty"`
	UserId       int    `json:"user_id,omitempty"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    *time.Time `sql:"index"`
}
