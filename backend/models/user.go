package models

import "time"

type Users struct {
	ID          int        `gorm:"PRIMARY_KEY" json:"id"`
	Username    string     `json:"username"`
	Password    string     `json:"password"`
	Firstname   string     `json:"firstname"`
	Lastname    string     `json:"lastname"`
	Position    string     `json:"position"`
	PhoneNumber string     `json:"phone_number"`
	UserStatus  int        `json:"user_status"`
	CreatedAt   time.Time  `json:"created_at"`
	UpdatedAt   time.Time  `json:"updated_at"`
	DeletedAt   *time.Time `sql:"index"`
}

type UserStatuses struct {
	UserStatusId int    `gorm:"PRIMARY_KEY" json:"user_status_id"`
	UserStatus   string `json:"user_status"`
}
