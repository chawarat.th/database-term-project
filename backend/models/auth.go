package models

/*
เก็บ Struct ข้อมูล และ Method
สามารถสร้าง file.go เพื่อจัดกลุ่มของ model
เช่น

- Customer.go struct and method ที่เกี่ยวกับ customer  ไว้แล้วเซท package models
ชื่อStruct ตัวใหญ่นะ
*/
import "github.com/dgrijalva/jwt-go"

//TokenClaim strut body ของ token
type TokenClaim struct {
	ID         int        `json:"id"`
	Username   string     `json:"username"`
	Password   string     `json:"password"`
	Permission Permission `json:"permission_system"`
	Type       int        `json:"type"`
	jwt.StandardClaims
}

//AuthResponse ช้อมูลที่คืน
type AuthResponse struct {
	AccessToken  string `json:"access_token,omitempty"`
	TokenType    string `json:"token_type,omitempty"`
	ExpiresIn    string `json:"expires_in,omitempty"` // uint: seconds
	Id           int    `json:"id,omitempty"`
	Username     string `json:"username,omitempty"`
	FullName     string `json:"full_name,omitempty"`
	ProfileImage string `json:"profile_image"`
	Permission   string `json:"permission"`
}
type Password struct {
	DbPassword      string `json:"db_password,omitempty"`
	OldPassword     string `json:"old_password" validate:"required"`
	NewPassword     string `json:"new_password" validate:"eqfield=ConfirmPassword"`
	ConfirmPassword string `json:"confirm_password" validate:"required"`
}
