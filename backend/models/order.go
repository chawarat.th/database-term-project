package models

import "time"

type Order struct {
	OrderId                 int            `gorm:"PRIMARY_KEY;" json:"order_id"`
	RefCode                 string         `json:"ref_code"`
	OrderDate               time.Time      `json:"order_date"`
	UserId                  int            `json:"user_id"`
	StatusId                int            `json:"status_id"`
	OrderAddressNo          string         `json:"order_address_no"`
	OrderAddressMoo         string         `json:"order_address_moo"`
	OrderAddressStreet      string         `json:"order_address_street"`
	OrderAddressSubDistrict string         `json:"order_address_sub_district"`
	OrderAddressDistrict    string         `json:"order_address_district"`
	OrderAddressProvince    string         `json:"order_address_province"`
	OrderAddressZipcode     string         `json:"order_address_zipcode"`
	TotalPrice              int            `json:"total_price"`
	CreatedAt               time.Time      `json:"created_at"`
	UpdatedAt               time.Time      `json:"updated_at"`
	DeletedAt               *time.Time     `json:"deleted_at"`
	OrderDetails            []OrderDetails `gorm:"ForeignKey:OrderId;association_foreignkey:OrderId" json:"order_details"`
}

type OrderDetails struct {
	OrderDetailId int        `gorm:"PRIMARY_KEY;" json:"order_detail_id"`
	ProductId     int        `json:"product_id"`
	OrderId       int        `json:"order_id"`
	Quantity      int        `json:"quantity"`
	CreatedAt     time.Time  `json:"created_at"`
	UpdatedAt     time.Time  `json:"updated_at"`
	DeletedAt     *time.Time `json:"deleted_at"`
}

type OrderStatus struct {
	StatusId   int    `gorm:"PRIMARY_KEY;" json:"status_id"`
	StatusName string `json:"status_name"`
}

type OrderId struct {
	OrderId int `json:"order_id"`
}

func (OrderId) TableName() string {
	return "orders"
}

type ResponseOrder struct {
	OrderId             int                   `json:"order_id"`
	RefCode             string                `json:"ref_code"`
	OrderDate           string                `json:"order_date"`
	FullName            string                `json:"full_name"`
	StatusName          string                `json:"status_name"`
	TotalPrice          int                   `json:"total_price"`
	OrderAddress        string                `json:"order_address"`
	ResponseOrderDetail []ResponseOrderDetail `json:"order_details"`
}

type ResponseOrderDetail struct {
	OrderDetailId      int    `json:"order_detail_id"`
	ProductId          int    `json:"product_id"`
	ProductName        string `json:"product_name"`
	ProductPrice       int    `json:"product_price"`
	ProductDescription string `json:"product_description"`
	ProductImage       string `json:"product_image"`
	Quantity           int    `json:"quantity"`
}

type ResponseOrderList struct {
	OrderId    int    `json:"order_id"`
	RefCode    string `json:"ref_code"`
	OrderDate  string `json:"order_date"`
	StatusName string `json:"status_name"`
	StatusId   int    `json:"status_id"`
	TotalPrice int    `json:"total_price"`
}
