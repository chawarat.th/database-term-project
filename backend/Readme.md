####Go template initial for Echo V4 framework <br>

How to use
* Enable go modules integration in preference <br>
* Change module name in **go.mod** file
* Initial DB env in .env
* add `/{database-term-project}/.idea` to .gitignore
* run command `go run main.go`
---
Authentication system before user setup
* change user model
* change permission before use
* run `./auto_keys.sh` before use in **keys** folder
---
######Note : if error when import try 'go get ....' or 'sync dependency'


