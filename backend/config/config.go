package config

/*
config ของโปรเจคลงนี่
*/
import (
	"crypto/rsa"
	"io/ioutil"
	"path/filepath"
	"time"

	"github.com/dgrijalva/jwt-go"
	_ "github.com/go-sql-driver/mysql"
)

// date Time format dd/mm/yy
var DATE = time.Now().Format("02/01/2006")

//GetKey อ่าน rsa key เอาไว้ทำ token
func GetKey() (*rsa.PrivateKey, *rsa.PublicKey) {
	//ตรวจสอบ path file key private
	keyPrivatePath, _ := filepath.Abs("./keys/key.rsa")
	// อ่านไฟล์
	key, err := ioutil.ReadFile(keyPrivatePath)
	if err != nil {
		panic(err)
	}
	// แปลงข้อมูลที่อ่านมาเป็น rsa private key
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		panic(err)
	}
	// key public
	keyPublicPath, _ := filepath.Abs("./keys/key.rsa.pub")
	key, err = ioutil.ReadFile(keyPublicPath)
	if err != nil {
		panic(err)
	}
	// แปลงข้อมูลที่อ่านมาเป็น rsa public key
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		panic(err)
	}
	return privateKey, publicKey
}
