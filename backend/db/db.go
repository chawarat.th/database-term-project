package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"os"
	"path/filepath"
)

func Init(env string) {
	switch env {
	case "prod":
		path, _ := filepath.Abs("./config/prod.env")
		err := godotenv.Load(path)
		_, _ = ConnectDataBase()
		if err != nil {
			panic(err)
		}
	}

}

var SQL *gorm.DB

func ConnectDataBase() (*gorm.DB,error) {
	connectionString := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"))

	db, err := gorm.Open("mysql", connectionString)
	if err != nil {
		return nil,err
	} else {
		fmt.Println("Connect Database Success")
	}
	return db,nil
}