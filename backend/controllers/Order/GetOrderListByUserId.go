package Order

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

func GetOrderListByUserId(c echo.Context) error {
	token := c.Get("user").(*jwt.Token)
	userId := token.Claims.(*models.TokenClaim).ID
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	resOrderList := new([]models.ResponseOrderList)
	strQuery := fmt.Sprintf(`
	SELECT
		order_id,
		ref_code,
		total_price,
		SUBSTRING(order_date,1,10) AS order_date,
		order_status.status_name
	FROM orders
	LEFT JOIN order_status ON order_status.status_id = orders.status_id
	WHERE orders.deleted_at IS NULL AND orders.user_id = %d ORDER BY order_id DESC`, userId)
	responseListOrder := connDb.Raw(strQuery).Scan(&resOrderList)
	if responseListOrder.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
			Result:  responseListOrder.Error.Error(),
		})
	}
	if responseListOrder.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
		})
	}
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "order list founded",
		Result:  resOrderList,
	})
}
