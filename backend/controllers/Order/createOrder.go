package Order

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func CreateOrder(c echo.Context) error {
	token := c.Get("user").(*jwt.Token)
	userId := token.Claims.(*models.TokenClaim).ID
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	orders := new(models.Order)
	bindOrder := c.Bind(&orders)
	if bindOrder != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant bind order",
			Result:  bindOrder.Error(),
		})
	}

	intRand := rand.Int31()
	strRand := strconv.Itoa(int(intRand))
	strRand = "OR-" + strRand
	refCode := strings.Replace(strRand, " ", "", -1)

	orders.UserId = userId
	orders.OrderDate = time.Now()
	orders.RefCode = refCode

	transaction := connDb.Begin()
	createOrder := transaction.Create(&orders)
	if createOrder.Error != nil {
		transaction.Rollback()
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant create order",
			Result:  createOrder.Error.Error(),
		})
	}
	for _, dataOrderDetail := range orders.OrderDetails {
		fmt.Println("Product ID :", dataOrderDetail.ProductId)
		product := new(models.Products)
		checkStock := transaction.Debug().Select("product_stock").Where("product_id = ?", dataOrderDetail.ProductId).Find(&product)
		if checkStock.Error != nil {
			transaction.Rollback()
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant found product",
				Result:  checkStock.Error.Error(),
			})
		}
		if product.ProductStock < dataOrderDetail.Quantity {
			transaction.Rollback()
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "low stock",
			})
		}
		if product.ProductStock == 0 {
			transaction.Rollback()
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "out of stock",
			})
		}
		updateProductStock := product.ProductStock - dataOrderDetail.Quantity
		cutStock := transaction.Debug().Exec("UPDATE products SET product_stock = ? WHERE product_id = ?", updateProductStock, dataOrderDetail.ProductId)
		if cutStock.Error != nil {
			transaction.Rollback()
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant cut stock",
				Result:  cutStock.Error.Error(),
			})
		}
	}
	transaction.Commit()
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "orders created",
	})
}
