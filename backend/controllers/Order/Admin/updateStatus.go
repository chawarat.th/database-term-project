package Admin

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
)

func UpdateStatus(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	paramStatusId := c.QueryParam("status_id")
	paramOrderId := c.QueryParam("order_id")
	transaction := connDb.Begin()

	orderCancel := new(models.Order)
	checkCancelOrder := transaction.Select("status_id").Where("order_id = ?", paramOrderId).Find(&orderCancel)
	if checkCancelOrder.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
			Result: checkCancelOrder.Error.Error(),
		})
	}
	if orderCancel.StatusId == 4 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant update status",
		})
	}


	strSqlUpdateOrder := fmt.Sprintf(`UPDATE orders SET status_id = %s WHERE deleted_at IS NULL AND order_id = %s`, paramStatusId, paramOrderId)
	if paramStatusId == "4" {
		strFoundOrderDetail := fmt.Sprintf(`SELECT * FROM order_details WHERE order_details.deleted_at IS NULL AND order_id = %s`, paramOrderId)
		orderDetails := new([]models.OrderDetails)
		foundOrderDetailId := transaction.Raw(strFoundOrderDetail).Scan(&orderDetails)
		if foundOrderDetailId.Error != nil {
			transaction.Rollback()
			fmt.Println("Error foundOrderDetailId", foundOrderDetailId.Error.Error())
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant found order detail",
				Result:  foundOrderDetailId.Error.Error(),
			})
		}

		for _, dataOrderDetail := range *orderDetails {
			product := new(models.Products)
			checkStock := transaction.Debug().Select("product_stock").Where("product_id = ?", dataOrderDetail.ProductId).Find(&product)
			if checkStock.Error != nil {
				transaction.Rollback()
				fmt.Println("Error checkStock", checkStock.Error.Error())
				return c.JSON(400, models.ResponseMessage{
					Status:  "Error",
					Message: "cant found product",
					Result:  checkStock.Error.Error(),
				})
			}
			calStock := product.ProductStock + dataOrderDetail.Quantity
			strSqlUpdateStockProduct := fmt.Sprintf(`UPDATE products SET product_stock = %d WHERE product_id = %d`, calStock, dataOrderDetail.ProductId)
			cutStock := transaction.Debug().Exec(strSqlUpdateStockProduct)
			if cutStock.Error != nil {
				transaction.Rollback()
				fmt.Println("Error cutStock", cutStock.Error.Error())
				return c.JSON(400, models.ResponseMessage{
					Status:  "Error",
					Message: "cant update stock",
					Result:  cutStock.Error.Error(),
				})
			}
			deleteOrderDetail := transaction.Where("order_detail_id = ?", dataOrderDetail.OrderDetailId).Delete(&orderDetails)
			if deleteOrderDetail.Error != nil {
				transaction.Rollback()
				fmt.Println("Error deleteOrderDetail", deleteOrderDetail.Error.Error())
				return c.JSON(400, models.ResponseMessage{
					Status:  "Error",
					Message: "cant delete order detail id",
					Result:  deleteOrderDetail.Error.Error(),
				})
			}
		}
		updateOrderCancelStatus := transaction.Debug().Exec(strSqlUpdateOrder)
		if updateOrderCancelStatus.Error != nil {
			transaction.Rollback()
			fmt.Println("Error updateOrderCancelStatus", updateOrderCancelStatus.Error.Error())
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant update order status",
				Result:  updateOrderCancelStatus.Error.Error(),
			})
		}
	} else {
		updateOrderStatus := transaction.Debug().Exec(strSqlUpdateOrder)
		if updateOrderStatus.Error != nil {
			transaction.Rollback()
			fmt.Println("Error updateOrderStatus", updateOrderStatus.Error.Error())
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant update order status",
				Result:  updateOrderStatus.Error.Error(),
			})
		}
	}
	transaction.Commit()
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "status updated",
	})
}
