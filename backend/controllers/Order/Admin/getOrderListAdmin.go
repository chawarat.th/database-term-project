package Admin

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
)

func GetOrderListAdmin(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	resOrderList := new([]models.ResponseOrderList)
	strQuery := fmt.Sprintf(`
	SELECT
		order_id,
		ref_code,
		SUBSTRING(order_date,1,10) AS order_date,
		order_status.status_name,
		order_status.status_id,
		total_price
	FROM orders
	LEFT JOIN order_status ON order_status.status_id = orders.status_id
	WHERE orders.deleted_at IS NULL
	ORDER BY order_id DESC`)
	responseListOrder := connDb.Raw(strQuery).Scan(&resOrderList)
	if responseListOrder.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
			Result:  responseListOrder.Error.Error(),
		})
	}
	if responseListOrder.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
		})
	}
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "order list founded",
		Result: resOrderList,
	})
}
