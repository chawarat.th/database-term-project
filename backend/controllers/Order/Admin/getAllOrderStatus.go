package Admin

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
)

func GetAllOrderStatus(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()
	orderStatus := new([]models.OrderStatus)
	strFoundAllOrder := fmt.Sprintf(`SELECT * FROM order_status`)
	resultFoundOrderStatus := connDb.Raw(strFoundAllOrder).Scan(&orderStatus)
	if resultFoundOrderStatus.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant get order status",
			Result:  resultFoundOrderStatus.Error.Error(),
		})
	}
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "order status founded",
		Result:  orderStatus,
	})
}
