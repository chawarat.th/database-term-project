package Order

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
)

func GetOrderDetail(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()
	paramOrderId := c.QueryParam("order_id")
	responseOrder := new(models.ResponseOrder)
	responseOrderDetail := new([]models.ResponseOrderDetail)
	strQueryOrder := fmt.Sprintf(`SELECT order_id,
		ref_code,
		SUBSTRING(order_date,1,10) AS order_date,
		order_status.status_name,
		total_price,
		CONCAT(order_address_no,' ',order_address_moo,' ',order_address_street,' ',order_address_sub_district,' ',order_address_district,' ',order_address_province,' ',order_address_zipcode) AS order_address,
		CONCAT(users.firstname,' ',users.lastname) AS full_name
		FROM orders
		LEFT JOIN order_status ON order_status.status_id = orders.status_id
		LEFT JOIN users ON users.id = orders.user_id
		WHERE orders.order_id = %s`,paramOrderId)
	foundOrder := connDb.Raw(strQueryOrder).Scan(&responseOrder)
	if foundOrder.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
			Result:  foundOrder.Error.Error(),
		})
	}

	if foundOrder.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "order not found",
		})
	}

	strQueryOrderDetail := fmt.Sprintf(`SELECT
		order_detail_id,
		order_details.product_id,
		product_name,
		product_price,
		product_description,
		product_image,
		quantity
		FROM order_details
		LEFT JOIN products ON products.product_id = order_details.product_id
		WHERE order_details.deleted_at IS NULL AND order_details.order_id = %s`,paramOrderId)

	foundOrderDetail := connDb.Raw(strQueryOrderDetail).Scan(&responseOrderDetail)
	if foundOrderDetail.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found order",
			Result:  foundOrderDetail.Error.Error(),
		})
	}

	if foundOrderDetail.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "order not found",
		})
	}
	responseOrder.ResponseOrderDetail = *responseOrderDetail
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "order founded",
		Result:  responseOrder,
	})
}
