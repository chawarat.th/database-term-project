package CustomFunction

import (
	"crypto/sha256"
	"fmt"
)

func HashPassword(loginPassword string) string {
	h := sha256.New()
	h.Write([]byte(loginPassword))
	hash := fmt.Sprintf("%x", h.Sum(nil))
	return hash
}
