package Authentication

import (
	"database-term-project/controllers/CustomFunction"
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

func Login(c echo.Context) error {
	connDb, _ := db.ConnectDataBase()
	defer connDb.Close()

	user := new(models.Users)
	bindLogin := c.Bind(&user)
	if bindLogin != nil {
		return c.JSON(http.StatusBadRequest, models.ResponseMessage{
			Status:  "Error",
			Message: "cant bind value",
			Result:  bindLogin.Error(),
		})
	}
	loginPassword := user.Password //Input

	findUser := connDb.Raw("SELECT * FROM users WHERE username = ?", user.Username).Scan(&user)
	if user.DeletedAt != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "account is non-active",
		})
	}
	if findUser.RowsAffected == 0 {
		return c.JSON(http.StatusBadRequest, models.ResponseMessage{
			Status:  "Error",
			Message: "user-not-found",
			Result:  findUser.Error.Error(),
		})
	}
	userPassword := user.Password //Database
	comparePass := CustomFunction.ComparePassword(userPassword, loginPassword)
	if comparePass == false {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"message": "invalid-password",
			"error":   "error",
		})
	}
	permission := new(models.Permission)
	resultFindPermission := connDb.Where("user_id = ?", user.ID).Find(&permission)
	if resultFindPermission.Error != nil {
		return c.JSON(http.StatusOK, echo.Map{
			"message": "No Permission to access it",
			"err":     resultFindPermission.Error.Error(),
		})
	}
	token, expired := GenToken(user, tokenExpired, 1, permission)
	tokenExpired := time.Unix(expired, 0).Format("15:04:05 _2 Jan 2006")

	if user.UserStatus == 2 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "user-got-banned",
		})
	}
	return c.JSON(http.StatusOK, models.ResponseMessage{
		Status:  "Success",
		Message: "login success",
		Result: models.AuthResponse{
			AccessToken: token,
			ExpiresIn:   tokenExpired,
			Id:          user.ID,
			FullName:    user.Firstname + " " + user.Lastname,
			Permission:  user.Position,
		},
	})

}
