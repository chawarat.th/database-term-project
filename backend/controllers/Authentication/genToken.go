package Authentication

import (
	"github.com/dgrijalva/jwt-go"
	"database-term-project/config"
	"database-term-project/models"
	"time"
)

const tokenExpired = time.Duration(time.Hour * 5)

func GenToken(data *models.Users, expiresIn time.Duration, tokenType int, permission *models.Permission) (string, int64) {
	expiresAt := int64(0)
	now := time.Now()
	if expiresIn > 0 {
		expiresAt = now.Add(expiresIn).Unix()
	}

	tokenClaim := jwt.NewWithClaims(jwt.SigningMethodRS256, models.TokenClaim{
		ID:         data.ID,
		Username:   data.Username,
		Permission: *permission,
		Type:       tokenType,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: expiresAt,
		},
	})
	privateKey, _ := config.GetKey()
	token, err := tokenClaim.SignedString(privateKey)
	if err != nil {
		panic(err)
	}
	return token, expiresAt
}
