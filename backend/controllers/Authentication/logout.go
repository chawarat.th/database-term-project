package Authentication

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"net/http"
	"database-term-project/db"
	"database-term-project/models"
)

func Logout(c echo.Context) error {
	conDB, errConn := db.ConnectDataBase()
	defer conDB.Close()
	if errConn != nil {
		return c.JSON(http.StatusBadRequest, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect to database",
			Result:  errConn.Error(),
		})
	}

	token := c.Get("user").(*jwt.Token)
	userId := token.Claims.(*models.TokenClaim).ID
	fmt.Println(userId)
	return c.JSON(http.StatusOK, models.ResponseMessage{
		Status:  "Success",
	})
}
