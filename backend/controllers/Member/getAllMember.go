package Member

import (
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
	"net/http"
)

func GetAllMember(c echo.Context) error {
	response := new(models.ResponseMessage)
	connDb, err := db.ConnectDataBase()
	if err != nil {
		response.Status = "Error"
		response.Message = "cant connect database"
		response.Result = err
		return c.JSON(400,response)
	}
	defer connDb.Close()
	users := new([]models.Users)
	resultGetAllMember := connDb.Find(&users)
	if resultGetAllMember.RowsAffected == 0{
		response.Result = "Error"
		response.Message = "cant found users"
		return c.JSON(400, response)
	}
	if resultGetAllMember.Error != nil {
		response.Status = "Error"
		response.Message = "cant found users"
		response.Result = resultGetAllMember.Error.Error()
		return c.JSON(400, response)
	}

	return c.JSON(http.StatusOK, models.ResponseMessage{
		Status:  "Success",
		Message: "user found",
		Result:  users,
	})
}
