package Member

import (
	"database-term-project/controllers/CustomFunction"
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
	"net/http"
	"strings"
)

func Registration(c echo.Context) error {
	connDb, err := db.ConnectDataBase()
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  err.Error(),
		})
	}
	defer connDb.Close()
	register := new(models.BindRegister)
	if err := c.Bind(register); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	//phoneExp := regexp.MustCompile("^(?:(?:\\+|00)33[\\s.-]{0,3}(?:\\(0\\)[\\s.-]{0,3})?|0)[1-9](?:(?:[\\s.-]?\\d{2}){4}|\\d{2}(?:[\\s.-]?\\d{3}){2})$")
	//validatePhone := phoneExp.MatchString(register.PhoneNumber)
	//fmt.Println(validatePhone)
	//if !validatePhone {
		//return c.JSON(400, models.ResponseMessage{
		//	Status:  "Error",
		//	Message: "wrong phone number pattern",
	//	})
	//}
	if register.Password != register.ConfirmPassword {
		return c.JSON(http.StatusBadRequest, models.ResponseMessage{
			Status:  "Error",
			Message: "password-not-match",
		})
	}
	// แสดงรหัสผ่านแค่ 3 ตัวสุดท้าย
	hideLength := len(register.Password) - 3
	hidePassword := strings.Repeat("*", hideLength) + register.Password[hideLength:len(register.Password)]

	// ตัดชื่อ - นามสกุล
	//resultSplit := strings.Split(register.FullName," ")
	// hash รหัสผ่าน
	password := CustomFunction.HashPassword(register.Password)

	user := new(models.Users)
	user.Username = register.Username
	user.Password = password
	user.Firstname = register.FirstName
	user.Lastname = register.LastName
	user.Position = register.Position
	user.UserStatus = 1
	user.Position = "user"

	resultSaveUser := connDb.Save(&user)
	if resultSaveUser.Error != nil {
		if strings.Contains(resultSaveUser.Error.Error(), "Error 1062") {
			return c.JSON(http.StatusBadRequest, models.ResponseMessage{
				Status:  "Error",
				Message: "duplicate username",
				Result:  resultSaveUser.Error.Error(),
			})
		} else {
			return c.JSON(http.StatusBadRequest, models.ResponseMessage{
				Status:  "Error",
				Message: "cant register",
				Result:  resultSaveUser.Error.Error(),
			})
		}
	}

	permission := new(models.Permission)
	if user.Position == "admin" {
		permission.Admin = "active"
	} else if user.Position == "user" {
		permission.User = "active"
	}
	permission.UserId = user.ID
	resultSavePermission := connDb.Save(&permission)
	if resultSavePermission.Error != nil {
		return c.JSON(http.StatusBadRequest, resultSavePermission.Error.Error())
	}

	return c.JSON(http.StatusOK, models.ResponseMessage{
		Status:  "Success",
		Message: "registered",
		Result: echo.Map{
			"message":  "success",
			"fullname": user.Firstname + " " + user.Lastname,
			"Username": user.Username,
			"Password": hidePassword,
		},
	})
}
