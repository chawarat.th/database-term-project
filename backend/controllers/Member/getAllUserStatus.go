package Member

import (
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
)

func GetAllUserStatus(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	userStatus := new([]models.UserStatuses)
	resultFoundUserStatus := connDb.Find(userStatus)
	if resultFoundUserStatus.Error != nil {
		return c.JSON(400,models.ResponseMessage{
			Status:  "Error",
			Message: "cant found user status",
			Result:  resultFoundUserStatus.Error.Error(),
		})
	}

	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "dropdown all status",
		Result:  userStatus,
	})
}
