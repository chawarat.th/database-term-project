package Member

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
)

func UpdateUserStatus(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	paramUserStatusId := c.QueryParam("user_status_id")
	paramUserId := c.QueryParam("user_id")

strUpdateUserStatus := fmt.Sprintf(`UPDATE users SET user_status = %s WHERE deleted_at IS NULL AND id = %s`,paramUserStatusId, paramUserId)
	updateUserStatus := connDb.Exec(strUpdateUserStatus)
	if updateUserStatus.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant update user status",
			Result:  updateUserStatus.Error.Error(),
		})
	}
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "user status updated",
	})
}
