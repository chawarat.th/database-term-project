package ProductType

import (
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
)

func GetAllProductType(c echo.Context) error {
	connDb, _ := db.ConnectDataBase()
	defer connDb.Close()


	productType := new([]models.ProductTypes)
	foundProductType := connDb.Raw("SELECT * FROM product_types WHERE deleted_at IS NULL ORDER BY type_id DESC").Scan(&productType)
	if foundProductType.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found product type",
			Result:  foundProductType.Error.Error(),
		})
	}

	if foundProductType.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "product type not found",
		})
	}

	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "product type founded",
		Result:  productType,
	})
}
