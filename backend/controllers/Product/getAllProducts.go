package Product

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
	"strconv"
)

func GetAllProducts(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()
	products := new([]models.Products)
	paramTypeId := c.QueryParam("type_id")
	pagination := new(models.Pagination)
	pagination.Limit, _ = strconv.Atoi(c.QueryParam("limit"))
	pagination.Page, _ = strconv.Atoi(c.QueryParam("page"))
	offset := (pagination.Limit * (pagination.Page)) - pagination.Limit

	strQuery := ""
	sqlCount := ""
	if paramTypeId == "13" {
		sqlCount = fmt.Sprintf(`SELECT CEIL(COUNT(*) ) AS total_rows FROM products WHERE deleted_at IS NULL`)
		strQuery = fmt.Sprintf(`SELECT * FROM products WHERE deleted_at IS NULL ORDER BY product_id DESC`)
	} else {
		sqlCount = fmt.Sprintf(`SELECT CEIL(COUNT(*) ) AS total_rows FROM products WHERE deleted_at IS NULL AND type_id = %s`, paramTypeId)
		strQuery = fmt.Sprintf(`SELECT * FROM products WHERE deleted_at IS NULL AND type_id = %s ORDER BY product_id DESC`, paramTypeId)

	}
	foundAllProduct := connDb.Raw(strQuery+" LIMIT ? OFFSET ?", pagination.Limit, offset).Scan(&products)
	if foundAllProduct.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found product",
			Result:  foundAllProduct.Error.Error(),
		})
	}
	if foundAllProduct.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "product not found",
		})
	}

	countProduct := connDb.Raw(sqlCount).Scan(&pagination)
	if countProduct.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found product",
			Result:  foundAllProduct.Error.Error(),
		})
	}

	if countProduct.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "product not found",
		})
	}
	pagination.Rows = products
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "all-product-founded",
		Result:  pagination,
	})
}
