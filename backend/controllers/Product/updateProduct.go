package Product

import (
	"database-term-project/controllers/CustomFunction"
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
	"strconv"
	"time"
)

func UpdateProduct(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	paramProductId := c.QueryParam("product_id")
	product := new(models.Products)
	product.ProductName = c.FormValue("product_name")
	product.ProductPrice, _ = strconv.Atoi(c.FormValue("product_price"))
	product.ProductDescription = c.FormValue("product_description")
	product.ProductStock, _ = strconv.Atoi(c.FormValue("product_stock"))
	product.TypeId, _ = strconv.Atoi(c.FormValue("type_id"))

	multipartForm, _ := c.MultipartForm()
	productImage := multipartForm.File["product_image"]
	var conditionImg []CustomFunction.ExtensionFile
	conditionImg = append(conditionImg, CustomFunction.IsPng)
	conditionImg = append(conditionImg, CustomFunction.IsJpeg)
	conditionImg = append(conditionImg, CustomFunction.IsJpg)

	if productImage != nil {
		uploadProductImage := CustomFunction.NewUploadService()
		destinationProductImg := CustomFunction.ProfileImages
		fileObjImg := new(CustomFunction.FileObj)

		errUploadImg := CustomFunction.UploadFile(uploadProductImage, productImage[0], destinationProductImg, conditionImg, fileObjImg)
		if errUploadImg != nil {
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant upload",
				Result:  errUploadImg,
			})
		}
		product.ProductImage = fileObjImg.FileUrl
		updateProductImage := connDb.Exec("UPDATE products SET product_image = ? WHERE product_id = ? AND deleted_at IS NULL", product.ProductImage, paramProductId)
		if updateProductImage.Error != nil {
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant update new product image",
				Result:  updateProductImage.Error.Error(),
			})
		}

	}

	updateProduct := connDb.Exec("UPDATE products SET "+
		"product_name = ?, "+
		"product_price = ?, "+
		"product_description = ?, "+
		"product_stock = ?, "+
		"type_id = ?, "+
		"updated_at = ? WHERE product_id = ? AND deleted_at IS NULL",
		product.ProductName,
		product.ProductPrice,
		product.ProductDescription,
		product.ProductStock,
		product.TypeId,
		time.Now(),
		paramProductId)
	if updateProduct.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant update product",
			Result:  updateProduct.Error.Error(),
		})
	}

	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "product updated",
	})
}
