package Product

import (
	"database-term-project/db"
	"database-term-project/models"
	"fmt"
	"github.com/labstack/echo/v4"
	"time"
)

func DeleteProduct(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()
	paramProductId := c.QueryParam("product_id")
	transaction := connDb.Begin()

	orderDetail := new([]models.OrderDetails)
	strHasStock := fmt.Sprintf(`
		SELECT 
			product_id, 
			orders.status_id 
		FROM order_details 
		LEFT JOIN orders ON orders.order_id = order_details.order_id
		WHERE order_details.deleted_at IS NULL
		AND orders.status_id NOT IN (1,2,3) AND product_id = %s`, paramProductId)
	checkHasStock := transaction.Debug().Raw(strHasStock).Scan(&orderDetail)
	if checkHasStock.RowsAffected > 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant delete product please complete order first",
			Result:  checkHasStock.Error,
		})
	}
deleteOrderDetail := transaction.Debug().Where("product_id = ?", paramProductId).Delete(&orderDetail)
	if deleteOrderDetail.Error != nil {
		transaction.Rollback()
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant delete product order detail",
			Result:  deleteOrderDetail.Error.Error(),
		})
	}
	deleteProduct := transaction.Debug().Exec("UPDATE products SET deleted_at = ? WHERE product_id = ? AND deleted_at IS NULL", time.Now(), paramProductId)
	if deleteProduct.Error != nil {
		transaction.Rollback()
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant delete product",
			Result:  deleteProduct.Error.Error(),
		})
	}
	if deleteProduct.RowsAffected == 0 {
		transaction.Rollback()
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant delete product",
			Result:  deleteProduct.Error.Error(),
		})
	}
	transaction.Commit()
	return c.JSON(200,models.ResponseMessage{
		Status:  "Success",
		Message: "product deleted",
	})
}
