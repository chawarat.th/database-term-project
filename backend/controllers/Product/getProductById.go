package Product

import (
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
)

func GetProductById(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()
	paramProductId := c.QueryParam("product_id")
	product := new(models.Products)
	
	getProduct := connDb.Raw("SELECT * FROM products WHERE product_id = ? AND deleted_at IS NULL",paramProductId).Scan(&product)
	if getProduct.RowsAffected == 0 {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "product not found",
		})
	}
	if getProduct.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant found product by id",
			Result:  getProduct.Error.Error(),
		})
	}
	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "product ",
		Result:  product,
	})
}
