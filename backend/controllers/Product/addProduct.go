package Product

import (
	"database-term-project/controllers/CustomFunction"
	"database-term-project/db"
	"database-term-project/models"
	"github.com/labstack/echo/v4"
	"strconv"
	"time"
)

func AddProduct(c echo.Context) error {
	connDb, connErr := db.ConnectDataBase()
	if connErr != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant connect database",
			Result:  connErr,
		})
	}
	defer connDb.Close()

	product := new(models.Products)
	product.ProductName = c.FormValue("product_name")
	product.ProductPrice, _ = strconv.Atoi(c.FormValue("product_price"))
	product.ProductDescription = c.FormValue("product_description")
	product.ProductStock, _ = strconv.Atoi(c.FormValue("product_stock"))
	product.TypeId, _ = strconv.Atoi(c.FormValue("type_id"))

	multipartForm, _ := c.MultipartForm()
	productImage := multipartForm.File["product_image"]
	var conditionImg []CustomFunction.ExtensionFile
	conditionImg = append(conditionImg, CustomFunction.IsPng)
	conditionImg = append(conditionImg, CustomFunction.IsJpeg)
	conditionImg = append(conditionImg, CustomFunction.IsJpg)

	if productImage != nil {
		uploadProductImage := CustomFunction.NewUploadService()
		destinationProductImg := CustomFunction.ProfileImages
		fileObjImg := new(CustomFunction.FileObj)

		errUploadImg := CustomFunction.UploadFile(uploadProductImage, productImage[0], destinationProductImg, conditionImg, fileObjImg)
		if errUploadImg != nil {
			return c.JSON(400, models.ResponseMessage{
				Status:  "Error",
				Message: "cant upload",
				Result:  errUploadImg,
			})
		}
		product.ProductImage = fileObjImg.FileUrl
	}

	insertProduct := connDb.Exec("INSERT INTO products "+
		"(product_name, "+
		"product_price, "+
		"product_description, "+
		"product_stock, "+
		"product_image, "+
		"type_id, "+
		"created_at, "+
		"updated_at) VALUES "+
		"(?,?,?,?,?,?,?,?) ",
		product.ProductName, product.ProductPrice, product.ProductDescription, product.ProductStock, product.ProductImage, product.TypeId, time.Now(), time.Now())
	if insertProduct.Error != nil {
		return c.JSON(400, models.ResponseMessage{
			Status:  "Error",
			Message: "cant insert product",
			Result:  insertProduct.Error.Error(),
		})
	}

	return c.JSON(200, models.ResponseMessage{
		Status:  "Success",
		Message: "created product",
	})
}
